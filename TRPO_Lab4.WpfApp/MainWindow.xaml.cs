﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TRPO_Lab3.Lib;




namespace TRPO_Lab4.WpfApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new WMmod();
        }
        public class WMmod : INotifyPropertyChanged
        {
            public const double PI = 3.14;

            private double _height;
            public double Height
            {
                get { return _height; }
                set { _height = value; OnPropertyChanged("V"); }
            }

            private double _radius1;
            public double Radius1
            {
                get { return _radius1; }
                set
                { _radius1 = value; OnPropertyChanged("V"); }
            }
            private double _radius2;
            public double Radius2
            {
                get { return _radius2; }
                set
                { _radius2 = value; OnPropertyChanged("V"); }
            }

            public double V
            {
                get
                {
                    return Math.Round(Class1.Konus(Height, Radius1, Radius2), 2);
                }
            }
            public event PropertyChangedEventHandler PropertyChanged;
            protected virtual void OnPropertyChanged(string propertyName)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}