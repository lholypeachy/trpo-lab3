﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using TRPO_Lab3.Lib;
using WebApplication.Konus.Models;



namespace WebApplication.Konus.Controllers
{
    public class KonusController : Controller
    {
        [HttpGet]
        public ActionResult KonusV()
        {
            return View();
        }
        [HttpPost]
        public ActionResult KonusV(string h, string r1, string r2)
        {
            double V = Class1.Konus(Convert.ToDouble(h), Convert.ToDouble(r1), Convert.ToDouble(r2));
            ViewBag.Result = Math.Round(V, 3);
            return View();
        }
    }
}
