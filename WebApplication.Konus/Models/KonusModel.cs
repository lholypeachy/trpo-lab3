﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication.Konus.Models
{
    public class KonusModel
    {
        public double H { get; set; }

        public double R1 { get; set; }

        public double R2 { get; set; }

        public double V { get; set; }

        public const double Pi = 3.14;

    }
}
